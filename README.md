# Website-Template
## Dev gulp tasks: 
- assets - collect assets from assets dir and push them into ./dist/assets so they are avaliable through browser-sync
- sass - build scss files from scss dir into ./dist/style.css
- js - collect js files from js dir, concat them, build sourcemap into ./dist/js/scripts.js
- index - collect html files from template_parts dir and inject them into index.html through partial tags
### watch - all above with watch on files. Use `gulp watch` when developing

## Build gulp tasks:
### build - all files get minified and concatenated. Use `gulp build` if you want to build website for production 


