'use strict';
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var injectPartials = require('gulp-inject-partials');
const minify = require('gulp-minify');
const watch = require('gulp-watch'); 
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    minifycss = require('gulp-minify-css'),
    combineMq = require('gulp-combine-mq'),
    concat = require('gulp-concat'),
    runSequence = require('run-sequence'),
    htmlmin = require('gulp-htmlmin');

// --- [DEV TASKS] ---

gulp.task('assets', function() {
      return gulp.src('./assets/**/*').pipe(gulp.dest('./dist/assets/'));
});

gulp.task('browser-sync', function() {
      browserSync.init({
          server: {
              baseDir: "./dist/"
          }
      });
});

// SASS the heck out of this project:
gulp.task('sass', function() {
    return gulp.src('./scss/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            precision: 10
        }).on('error',sass.logError))
        .pipe(sourcemaps.write({includeContent: false}))
        .pipe(gulp.dest('./dist/'));
});

// Concatenate all JS into one script:
gulp.task('js', function() {
    return gulp.src('./js/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('scripts.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('index', function () {
      return gulp.src('./index.html')
      .pipe(injectPartials({
      removeTags: true
      }).on('error', (err) => {
          console.log(err.toString());

          this.emit('end');
      }))
      .pipe(gulp.dest('./dist/'));
});


gulp.task('index:watch', function () {
      watch('./*.html', () => {
          gulp.start('index')
      });
      watch('./template_parts/**/*.html', () => {
        gulp.start('index')
    });
})


// Watcher:
gulp.task('watch', function () {
    gulp.start('assets');
    watch('./assets/**/*', () => {
        gulp.start('assets');
    })
    // Create LiveReload server
    gulp.start('browser-sync');
    // Watch .scss files
    gulp.start('sass');
    watch('./scss/**/*.scss', () => {
        gulp.start('sass');
    });

    gulp.start('js')    
    watch('./js/**/*.js', () => {
        gulp.start('js')
    });

    gulp.start('index:watch');
});

// --- [BUILD TASKS] ---


gulp.task('build-index', function() {
      return gulp.src('./index.html')
      .pipe(injectPartials({
      removeTags: true
      }))
      .pipe(gulp.dest('./dist/'));
})

gulp.task('build-assets', function() {
      return gulp.src('./assets/**/*').pipe(gulp.dest('./dist/assets/'));
});

gulp.task('build-html', () => {
    return gulp.src('./dist/*.html')
      .pipe(htmlmin({ collapseWhitespace: true }))
      .pipe(gulp.dest('./dist/'));
});

// Make the CSS as small and optimized as possible:
gulp.task('build-css', function() {
    return gulp.src('./scss/style.scss')
        .pipe(sass({
            precision: 10,
            onError: console.error.bind(console, 'Sass error:')
        }))
        .pipe(autoprefixer())
        .pipe(combineMq())
        .pipe(minifycss())
        .pipe(gulp.dest('./dist/'));
});

// Concatenate and uglify JS:
gulp.task('build-js', function() {
    return gulp.src(['./js/*.js'])
      .pipe(concat('scripts.js'))
      .pipe(minify({
            ext:{
                  src:'-debug.js',
                  min:'.js'
              },
            noSource: true,
            ignoreFiles: ['.min.js']
      }))
      .pipe(gulp.dest('./dist/js/'));
});

// Setup the build:
gulp.task('build', function (cb) {
    // CSS and JavaScript:
    runSequence('build-css', 'build-js', 'build-index', 'build-html', 'build-assets', cb);
});